# Organ Donation
The Final Integrated Project - Organ Donation

# About
Organ Donation project

# Members
Since we are a team of 4, we have divided up the Project Manager role.
* [Fernando D'Oria](http://nandodoria.ca/) - *Animator and Video Editor*
* [Logan Litman](http://loganlitman.com/) - *Graphic Designer*
* [Carter Rose](http://carterrose.ca/) - *Front-End Developer*
* [Joanna Baile](http://joannabaile.com/) - *Back-End Developer*


## License
This project is licensed under the [MIT License](https://opensource.org/licenses/MIT/).
